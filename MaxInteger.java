//package moe.yun.cs422;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.StringUtils;

/**
 * Implement the max integer problem with MapReduce.
 * 
 * Create  3-4 short test files with integers, this will be the input to your program
 * 
 * Run your program, take a screen shot of the output
 * Provide a one page description of your implementation
 *   - Describe what classes you had to implement
 *   - What are the key/value pairs
 *   - Describe the test data that you created
 *   - Describe the output that you expect to see
 *   - Show the screen shot with the output of your program
 * 
 * Submit the code, only the classes that you implemented
 *   - Put in a lot of comments into your code
 *   - Explain in the comments what does each part your code do
 *   - Explain in the comments what does each method do
 *   - What are the key/value pairs
 * 
 * Run your code on an AWS instance
 * 
 * @author Gates_ice
 *
 */
public class MaxInteger {

	/**
	 * The Mapper class with four formal type parameters that specify the input key, input value,
	 * output key, output value.
	 *   - The input key is an integer offset.
	 *   - The input value is a line of text.
	 *   - The output key is the maximum within the input value.
	 *   - The output value is the name of file where input value from.
	 * @author Gates_ice
	 *
	 */
	public static class TokenizerMapper
		extends Mapper<Object, Text, IntWritable, Text>{

		private Text filename = new Text();						// Represents the input file.
		private String overall_maxfile = "";
		private final IntWritable one = new IntWritable(1);
		private IntWritable maximum = new IntWritable();		// Represents the maximum integer.
		private int overall_maximum = Integer.MIN_VALUE;		// Represents the overall maximum integer.
																// Initialize as the minimum of integer value.

		/**
		 * map() : Collect the maximum values for all input files.
		 * 
		 * The output key/value pairs may contains multiple results within the same input file.
		 * 
		 */
		@Override
		public void map(Object key, Text value, Context context) 
			throws IOException, InterruptedException {
			String line = value.toString();						// Get the line
			StringTokenizer itr = new StringTokenizer(line);	// Get the token
			String curfile = "";
			while (itr.hasMoreTokens()) {
				String token = itr.nextToken();					// Get the next token. It should be an integer, but it can be non-integer.
				try {
					int num = Integer.parseInt(token);			// Parse the token into integer.
					curfile = ((org.apache.hadoop.mapreduce.lib.input.FileSplit)context.getInputSplit()).getPath().getName();
																// If the token is not an integer, it will throw the exception
																// and we will get into the next token.
					if (overall_maximum <= num) {				// We have a larger number
						overall_maximum = num;
						
						overall_maxfile = curfile;				// tell that current file is the file with maximum integer.
						maximum.set(overall_maximum);			// write the number into the variable to be transfer to context.
					}
				} catch (NumberFormatException ex) {
					
				}
			}
			if (overall_maxfile.equals(curfile)) {				// figure out if current file has the maximum number
				filename.set(overall_maxfile);
				context.write(maximum, filename);				// write the filename into the variable to be transfer to context.
				overall_maxfile = "";
			}
		}
	}
	
	/**
	 * The Reducer class also have four formal type parameters.
	 *   - The input key and input value is the same as the Mapper output.
	 *   - The output key and value is the same as input one.
	 * 
	 * What result is expected?
	 *   - key: The overall maximum integer.
	 *   - value: Tells that which file(s) caintains the maximum integer.
	 *   
	 * @author Gates_ice
	 *
	 */
	public static class MaxIntegerReducer
		extends Reducer<IntWritable, Text, IntWritable, Text> {
		
		private Text overall_maxfile = new Text();				// Overall maximum file. (not used)
		private int overall_maximum = Integer.MIN_VALUE;		// Overall maximum integer.
		private final IntWritable one = new IntWritable(1);		
		private Vector<Text> max_files = new Vector<Text>();	// Files that contains the max integer.

		/**
		 * reduce(): Gather the key/value pairs from Mapper, collect them and get the final things.
		 * 
		 * We only need the maximum integer, so we will not write any result in this part.
		 * 
		 */
		public void reduce(IntWritable key, Iterable<Text> values, Context context ) 
			throws IOException, InterruptedException {
			
			if (key.get() > overall_maximum) {					// We find a larger 'Maximum Integer'.
				overall_maximum = key.get();
				max_files = new Vector<Text>();					// We need a container to store those filenames.
				for (Text val : values) {
					max_files.add(new Text(val));				// Add those filenames to the container.
				}
			}
		}

		/**
		 * cleanup(): Runs after reduce(). Do some works for the final results.
		 * 
		 * In this program, we will write the maximum integer into result.
		 * With the maximum integer, we will give out that which file(s) contains the integer.
		 * 
		 */
		public void cleanup(Context context) 
			throws IOException, InterruptedException {
			for (Text file : max_files) {						// Write all records.
				context.write(new IntWritable(overall_maximum), file);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();				// Configurations are specified by resources. A resource contains 
																// a set of name/value pairs as XML data. Each resource is named 
																// by either a String or by a Path. If named by a String, then the 
																// classpath is examined for a file with that name. If named by a 
																// Path, then the local filesystem is examined directly, without 
																// referring to the classpath.
		
		// GenericOptionsParser parse the commandline arguments for MaxInteger program.
		GenericOptionsParser optionParser = new GenericOptionsParser(conf, args);
		String[] remainingArgs = optionParser.getRemainingArgs();
		if (!(remainingArgs.length != 2 || remainingArgs.length != 4)) {
			System.err.println("Usage: maxinteger <in> <out>");
			System.exit(2);
		}
		
		// Job defines
		Job job = Job.getInstance(conf, "Max Integer");
		job.setJarByClass(MaxInteger.class);					
		job.setMapperClass(TokenizerMapper.class);				
		job.setCombinerClass(MaxIntegerReducer.class);
		job.setReducerClass(MaxIntegerReducer.class);
		job.setMapOutputKeyClass(IntWritable.class);			// Set the type of key/value pairs for Mapper.
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(IntWritable.class);				// Set the type of key/value pairs for program output.
		job.setOutputValueClass(Text.class);

		// Save arguments for program
		List<String> otherArgs = new ArrayList<String>();
		for (int i=0; i < remainingArgs.length; ++i) {
				otherArgs.add(remainingArgs[i]);
		}
		FileInputFormat.addInputPath(job, new Path(otherArgs.get(0)));
																// Set the input path, will read all files in that position.
		FileOutputFormat.setOutputPath(job, new Path(otherArgs.get(1)));
																// Set the output path, will read all files in that position.

		System.exit(job.waitForCompletion(true) ? 0 : 1);		// Wait and complete.
	}
}
